<?php
// Template Name: quem somos
?>

<?php get_header(); ?>
    <main class="main_quemsomos">
        <div class="title"> 
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="text">
            <p><?php the_content(); ?></p>
    </main>
    


<?php get_footer(); ?>